﻿using System.Reflection;
using uMod.Common;
using uMod.Extensions;
using uMod.Mock;

namespace uMod.MockGame
{
    public class MockProvider : uMod.Mock.MockProvider
    {
        
    }

    public class MockPlayer : uMod.Mock.MockPlayer
    {
        public MockPlayer(string playerId, string playerName) : base(playerId, playerName)
        {
        }

        public MockPlayer(GamePlayerIdentity player) : base(player)
        {
        }

        public MockPlayer(GamePlayer player) : base(player)
        {
        }
    }

    [GameExtension]
    public class MockExtension : Extension
    {
        internal static Assembly Assembly = Assembly.GetExecutingAssembly();
        internal static AssemblyName AssemblyName = Assembly.GetName();
        internal static VersionNumber AssemblyVersion = new VersionNumber(AssemblyName.Version.Major, AssemblyName.Version.Minor, AssemblyName.Version.Build);

        public override string Title => "Mock Game";
        public override string Author => "uMod Team and Contributors";
        public override VersionNumber Version => AssemblyVersion;

        public override string[] DefaultReferences { get; protected set; }
    }
}