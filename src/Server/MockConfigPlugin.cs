﻿using System.Collections.Generic;
using uMod.Common;
using uMod.Common.Web;
using uMod.Common.Command;
using System.Linq;
using System;

namespace uMod.Plugins
{
    [Info("Mock Config", "uMod", "1.0.0")]
    [Description("Mock config plugin")]
    public class MockConfigPlugin : Plugin
    {


        public enum TestEnum
        {
            Test,
            Test2
        }

        private void Loaded()
        {
            CallHook("OnMyHook", "hello", null);
        }

        private void OnMyHook(string name, TestEnum enum1)
        {
            Logger.Info("wat");
        }
    }
}