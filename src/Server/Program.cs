﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using uMod;
using uMod.Common;
using uMod.Configuration;
using uMod.Mock;
using uMod.Plugins;

namespace TestServer
{
    class EmptyBaseTest : BaseTest
    {
        public override IModule LoadModule()
        {
            return new Module(delegate(string msg)
            {
                // Store console log messages
            });
        }

        protected override IPlugin GetPlugin(Type type)
        {
            throw new NotImplementedException();
        }
    }

    class Program
    {
        private static bool _loading = true;
        private static Module _module;
        private static Stopwatch _timer;
        private static readonly BackgroundWorker InputWorker = new BackgroundWorker();
        private static string _input = "";

        private static IBatchPluginLoader pluginLoader;

        static void Main(string[] args)
        {
            _timer = new Stopwatch();
            _timer.Start();

            BaseTest.CurrentTest = new EmptyBaseTest();

            _module = BaseTest.CurrentTest.LoadModule() as Module;

            pluginLoader = new BatchPluginLoader(_module.RootLogger);

            BaseTest.CurrentTest.CurrentScope = new TestScope(ServerScope.PerTest)
            {
                Module = _module, PluginLoader = pluginLoader
            };

            pluginLoader.RegisterPlugin(typeof(MockGamePlugin));

            Interface.InitializeDebug(_module, new InitializationInfo()
            {
                Provider = typeof(uMod.MockGame.MockProvider)
            });

            _module.Extensions.RegisterLoader(pluginLoader);

            _module.OnLoaded.Add(Load);

            _module.RegisterEngineClock(EngineClock);

            float lastCall = _module.Now;

            while (_loading)
            {
                Thread.Sleep(25);
                float now = _module.Now;
                _module.OnFrame(now - lastCall);
                lastCall = now;
            }
        }

        static void Load()
        {
            _loading = false;

            StartInputWorker();

            float lastCall = _module.Now;

            _module.CallHook("IOnServerInitialized");

            while (!_module.IsShuttingDown)
            {
                if (!string.IsNullOrEmpty(_input))
                {
                    string input = _input;
                    _input = string.Empty;
                    MockProvider.CommandSystem.Handler.HandleConsoleMessage(MockServer.ServerPlayer, input);
                }
                Thread.Sleep(1);
                float now = _module.Now;
                _module.OnFrame(now - lastCall);
                lastCall = now;
            }
        }

        private static float EngineClock()
        {
            return (float)_timer.Elapsed.TotalSeconds;
        }

        private static void StartInputWorker()
        {
            InputWorker.DoWork += InputWorkerDoWork;
            InputWorker.RunWorkerCompleted += InputWorkerRunWorkerCompleted;

            InputWorker.RunWorkerAsync();
        }

        private static void InputWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            if (Console.KeyAvailable == false)
            {
                Thread.Sleep(100);
            }
            else
            {
                _input = Console.In.ReadLine();
            }
        }

        private static void InputWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(!InputWorker.IsBusy)
            {
                InputWorker.RunWorkerAsync();
            }
        }
    }
}
