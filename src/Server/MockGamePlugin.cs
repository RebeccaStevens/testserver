﻿using uMod.Plugins;
using uMod.Plugins.Decorators;

namespace uMod.Plugins
{
    [HookDecorator(typeof(ServerDecorator))]
    public class MockGamePlugin : Plugin
    {
        [Hook("OnServerInitialized")]
        private void OnServerInitialized()
        {
            Commands commands = new Commands();
            // Add universal commands
            commands.Initialize(this);

            object[] args = {
                "hello",
                1
            };

            timer.Repeat(0.1f, 0, delegate()
            {
                Interface.uMod.CallHook("HookSubbed", args);
            });

            timer.Repeat(0.1f, 0, delegate()
            {
                Interface.uMod.CallHook("HookSubbed", args);
            });

            timer.Repeat(0.1f, 0, delegate()
            {
                Interface.uMod.CallHook("HookSubbed", args);
            });

            timer.Repeat(0.1f, 0, delegate()
            {
                Interface.uMod.CallHook("HookSubbed", args);
            });
        }

        private object HookSubbed(string text, int num)
        {
            return null;
        }
    }
}